window.onload = main;

function main (){

	const username = localStorage.getItem("username");
	if (username !== null ) {
		document.getElementById('navbar-name').innerHTML = "Your products: " + username;
		getProducts((products) => {
			console.log(products);
		});
	} else {
		$('#login').modal('show');
	}
	// TODO: Authenticate user
	// TODO: Create session data
	// TODO: Show products filtered by user

	// document.getElementById("addProduct").addEventListener("click",onAddProduct);

	document.getElementById("logout").addEventListener("click",onLogout);

	document.getElementById("addProductSubmit").addEventListener("click",onAddProductSubmit);

	document.getElementById("submitloginUser").addEventListener("click", onSubmitLogin);
}

function onSubmitLogin(e){

	const user = document.getElementById("inputUser").value;
	const password = String(CryptoJS.MD5(document.getElementById("inputPass").value));

	login(user, password, (res_login) => {
		// show private data
		if (res_login.success){
			localStorage.setItem("username", user);
			// alert("show private data");
			$('#login').modal('hide');
			main();

		} else {
			alert("login error");
		}
	})
	e.preventDefault();

}

function getProducts(callback){

	$.ajax({
		url: "/product/list",
		method: "GET",
		datatype: "json",
		data: { "owner": username }
	}).done(callback);

}

function login(user, password, callback){

	$.ajax({
        url: "user/login",
        method: "POST",
        dataType: "json",
        data: { "user": user,
                "password" : password}
    }).done(callback);

}

function onLogout(){
	// TODO: Remove session data
	localStorage.removeItem("username");
	location.replace("index.html");
}

function onAddProductSubmit(e){
	product = {};

	let file = document.getElementById("inputImage").files[0];

	let img = file ? "http://localhost:9001/img/"+file.name : "";

	product.name = document.getElementById("inputName").value;
	product.description = document.getElementById("inputDescription").value;
	product.price = document.getElementById("inputPrice").value;
	product.img = img;

	console.log(product);
	
	if (file){
		console.log("File found")
		uploadImg(file, product, () => {
			addProduct(product, (res) => {
				console.log("Add product ? "+res.success);
			});		
		})
	} else {
		console.log("File not found")
		addProduct(product, (res) => {
			console.log("Add product ? "+res.success);
		});	
	}
	e.preventDefault();
	$('#addProductModal').modal('hide');
}

function addProduct(product, callback){

	$.ajax({
		url: "/product/add",
		method: "POST",
		datatype: "json",
		data: product
	}).done(callback);

}

function uploadImg(file, product, callback){
	
	let fd = new FormData();
	fd.append("productFile", file);

	$.ajax({
		url: "/product/upload-file",
		method: "POST",
		data: fd,
		processData: false,  // tell jQuery not to process the data
		contentType: false   // tell jQuery not to set contentType	  
	}).done(callback);

}