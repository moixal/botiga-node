import pg from 'pg'

const dbclient = new pg.Client({
    user: 'admin',
    host: 'localhost',
    database: 'botiga-node',
    password: 'admin',
    port: 5432
})

export default dbclient